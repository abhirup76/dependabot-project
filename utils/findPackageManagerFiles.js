const axios = require('axios');

const findPackageManagerFiles = async (projectId, gitlab_access_token, path = '', results = []) => {
    if(!gitlab_access_token){
        console.error("Please set the gitlab access token");
        return;
    }
    const HEADERS = {
        'PRIVATE-TOKEN': gitlab_access_token
    };
    const response = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/repository/tree`, {
        params: {
            path: path,
            per_page: 100
        },
        headers: HEADERS
    });

    const files = response.data;

    for (const file of files) {
        if (file.type === 'tree') {
            await findPackageManagerFiles(projectId, gitlab_access_token,file.path, results);
        } else {
            if (file.name === 'package.json') {
                results.push(("/"+file.path.substring(0, file.path.length - file.name.length - 1)));
            }
        }
    }
    return results;
};



module.exports = { findPackageManagerFiles }