require "json"
require "rubygems"
require "net/http"
require "graphql/client"
require "graphql/client/http"


#Variables for GraphQL to Github security advisory
HTTPVAR = GraphQL::Client::HTTP.new("https://api.github.com/graphql") do
  def headers(context)
    {
      "Authorization" => "Bearer #{ENV["GITHUB_ACCESS_TOKEN"]}",
      "Content-Type" => "application/json"
    }
  end
end

Schema = GraphQL::Client.load_schema(HTTPVAR)

Client = GraphQL::Client.new(schema: Schema, execute: HTTPVAR)

PackageVulnerabilityQuery = Client.parse <<-'GRAPHQL'
  query($package: String!) {
    securityVulnerabilities(ecosystem: NPM, package: $package, first: 10) {
      edges {
        node {
          vulnerableVersionRange
          severity
          advisory {
            description
            references {
              url
            }
          }
        }
      }
    }
  }
GRAPHQL

#Function to check if a given version is in a given range
def version_in_range?(version, range)
  requirement = Gem::Requirement.new(range.split(", "))
  requirement.satisfied_by?(Gem::Version.new(version))
end

# Function to check vulnerability, returns nil if there are no vulnerabilities, returns an object with vulnerability details otherwise
def check_vulnerability(package_name, version)
  response = Client.query(PackageVulnerabilityQuery, variables: { package: package_name })
  if response.errors.any?
    puts "Errors in checking security vulnerability : #{response.errors[:data]}"
    return nil
  end

  vulnerabilities = response.data.security_vulnerabilities.edges

  vulnerabilities.each do |vulnerability|
    node = vulnerability.node
    if version_in_range?(version, node.vulnerable_version_range)
      return {
        "VulnerableVersionRange" => node.vulnerable_version_range,
        "Severity" => node.severity,
        "Description" => node.advisory.description,
        "Links" => node.advisory.references
      }
    end
  end

  return nil
end

#Encoding for gitlab pipeline
Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

#Handling the whitelisted packages
file_path = './ignoreDependencies.json'

file_content = File.read(file_path)

@ignoreDependenciesHash = {}

begin
  @ignoreDependenciesHash = JSON.parse(file_content)
rescue => e
  puts "Error parsing json file"
end


#Calling the entrypoint script
require_relative('./generic-update-script')

#saving the hash into the file for whitelisted packages
@ignoreDependenciesHash.sort.to_h

json_string = JSON.pretty_generate(@ignoreDependenciesHash)

File.open(file_path, 'w') do |file|
  file.write(json_string)
end
