const { exec } = require('child_process');
const fs = require('fs');
require('dotenv').config();
const util = require('util');


// Configuration variables
const REPO_PATH = process.env.REPO_PATH || '../../..';
const GITLAB_ACCESS_TOKEN = process.env.GITLAB_ACCESS_TOKEN;
const BRANCH_NAME = process.env.BRANCH_NAME;

const switchBranchAndInstall = async () => {
  process.chdir(REPO_PATH);
  const folderPath = process.env.PROJECT_PATH.split('/').pop();

  if(!fs.existsSync(folderPath)){

    await runCommand(`git clone https://gitlab-ci-token:${GITLAB_ACCESS_TOKEN}@gitlab.com/${process.env.PROJECT_PATH}`);
    process.chdir(folderPath);
  }
  else {
    process.chdir(folderPath);
    await runCommand('git init');
    await runCommand(`git remote add origin https://gitlab-ci-token:${GITLAB_ACCESS_TOKEN}@gitlab.com/${process.env.PROJECT_PATH}`);
  }

  await runCommand('git fetch');
  await runCommand(`git checkout ${BRANCH_NAME}`);
  await runCommand('npm install');
  await runCommand('git add package-lock.json');
  await runCommand(`git commit -m 'package-lock.json updated'`);
  await runCommand(`git push origin ${BRANCH_NAME}`);
  process.chdir('../');
  fs.rm(folderPath,err=>{});
}


const runCommand = async (command) => {
  const execAsync = util.promisify(exec);
  try {
    const { stdout, stderr } = await execAsync(command);
    if (stderr) {
      // console.log(stderr);
    }
    // console.log(stdout);
  } catch (error) {
    // console.error(`Execution error: ${error}`);
  }
}

// Run the function
switchBranchAndInstall();

